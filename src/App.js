import Sport from "./Components/Sport";
import Technology from "./Components/Technology";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import TopHeadlines from "./Components/TopHeadlines";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<TopHeadlines />}/>
          <Route path="sport" element={<Sport />}/>
          <Route path="technology" element={<Technology />}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
